package com.test.jonnatas.viewModel

import com.test.jonnatas.domain.model.Currency
import com.test.jonnatas.domain.use_case.currency.ConvertCurrenciesUseCase
import com.test.jonnatas.domain.use_case.currency.FilterCurrencyUseCase
import com.test.jonnatas.domain.use_case.currency.GetCurrencyUseCase
import com.test.jonnatas.presentation.screens.currency.CurrencyViewModel
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

@OptIn(DelicateCoroutinesApi::class)
@ExperimentalCoroutinesApi
class CurrencyViewModelTest {
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    private lateinit var viewModel: CurrencyViewModel

    private val currency = mockk<Currency> {
        every { symbol } returns "UDS"
        every { name } returns "Dollar"
    }

    private val getCurrenciesUseCase: GetCurrencyUseCase = mockk(relaxed = true)
    private val convertCurrenciesUseCase: ConvertCurrenciesUseCase = mockk(relaxed = true)
    private val filterCurrencyUseCase: FilterCurrencyUseCase = mockk(relaxed = true)

    @Before
    fun setup() {
        Dispatchers.setMain(mainThreadSurrogate)
        MockKAnnotations.init(this)
        viewModel = CurrencyViewModel(
            getCurrenciesUseCase, convertCurrenciesUseCase, filterCurrencyUseCase
        )

    }

    @After
    fun tearDown() {
        Dispatchers.resetMain() // reset the main dispatcher to the original Main dispatcher
        mainThreadSurrogate.close()
    }

    @Test
    fun testChangeCurrencyEvent() {
        // initialize some test state
        return runTest {
            // code under test
            viewModel.changeCurrencyEvent(currency)
            val actual = viewModel.state.selectedCurrency
            assertEquals(currency, actual)
        }
    }

}
