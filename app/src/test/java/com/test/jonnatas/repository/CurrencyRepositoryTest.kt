package com.test.jonnatas.repository

import com.test.jonnatas.data.local.CurrencyDatabase
import com.test.jonnatas.data.local.CurrencyEntity
import com.test.jonnatas.data.local.RatesEntity
import com.test.jonnatas.data.remote.api.CurrencyApi
import com.test.jonnatas.data.repository.CurrencyRepositoryImpl
import com.test.jonnatas.domain.model.Currency
import com.test.jonnatas.domain.repository.CurrencyRepository
import com.test.jonnatas.listOfCurrency
import com.test.jonnatas.listOfRates
import io.mockk.Runs
import io.mockk.coEvery
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test


class CurrencyRepositoryTest {

    // The repository to be tested
    private lateinit var currencyRepository: CurrencyRepository

    // Mocks for any dependencies
    private val mapOfCurrency = mapOf(
        "USD" to "United States Dollar",
        "EUR" to "Euro",
    )
    private val apiMockk: CurrencyApi = mockk {
        coEvery { getCurrencies() } returns mapOfCurrency
        coEvery { getConvertedCurrencies() } returns mockk(relaxed = true)
    }
    private val dbMockk: CurrencyDatabase = mockk() {
        coEvery { dao } returns mockk {
            coEvery { getCurrencies() } returns listOfCurrency
            coEvery { getRates() } returns listOfRates
            coEvery { searchCurrency(any()) } returns listOf(mockk {
                every { name } returns "Euro"
                every { symbol } returns "EUR"
            })
        }
    }

    @Before
    fun setUp() {
        currencyRepository = CurrencyRepositoryImpl(
            api = apiMockk, db = dbMockk
        )
    }

    @Test
    fun testSyncDataRemote() {
        // Mock the behavior of the repository method
        runBlocking {
            coEvery { dbMockk.dao.getCurrencies() } returns listOfCurrency
            coEvery { dbMockk.dao.clearOldCurrencyList() } just Runs
            coEvery { dbMockk.dao.updateWithNewCurrencies(any()) } just Runs
            coEvery { dbMockk.dao.getRates() } returns listOfRates
            coEvery { dbMockk.dao.clearOldRatesList() } just Runs
            coEvery { dbMockk.dao.updateRates(any()) } just Runs

            val result = currencyRepository.getConvertedCurrencies()
            val actual = result.toList().first().data?.first()
            val expected = listOfRates.first()

            assertEquals(expected.symbol, actual?.symbol)
            assertEquals(expected.rate, actual?.exchangeRate)
        }
    }

    @Test
    fun testSyncDataError() {
        // Mock the behavior of the repository method
        runBlocking {
            coEvery { dbMockk.dao.getCurrencies() } returns emptyList() andThen emptyList()

            val result = currencyRepository.syncData()
            val actual = result.toList().first().message

            assertEquals("Couldn't load data", actual)
        }
    }


    @Test
    fun testGetCurrenciesRemote() {
        // Mock the behavior of the repository method
        runBlocking {

            val result = currencyRepository.getCurrencies()
            val actual = result.toList().first().data?.first()
            val expected = listOfCurrency.first()

            assertEquals(expected.name, actual?.name)
            assertEquals(expected.symbol, actual?.symbol)
        }
    }

    @Test
    fun testFilterCurrency() {
        // Mock the behavior of the repository method
        runBlocking {

            val result = currencyRepository.filterCurrency("E")
            val actual = result.toList().first().data?.first()
            val expected = Currency(symbol = "EUR", "Euro")

            assertEquals(expected.name, actual?.name)
            assertEquals(expected.symbol, actual?.symbol)
        }
    }

    @Test
    fun testGetCurrenciesLocal() {
        // Mock the behavior of the repository method
        runBlocking {
            coEvery { dbMockk.dao.getCurrencies() } returns emptyList() andThen listOfCurrency
            coEvery { dbMockk.dao.clearOldCurrencyList() } just Runs
            coEvery { dbMockk.dao.updateWithNewCurrencies(any()) } just Runs

            val result = currencyRepository.getCurrencies()
            val actual = result.toList().first().data?.first()
            val expected = listOfCurrency.first()

            assertEquals(expected.name, actual?.name)
            assertEquals(expected.symbol, actual?.symbol)
        }
    }

    @Test
    fun testGetCurrenciesError() {
        // Mock the behavior of the repository method
        runBlocking {
            coEvery { dbMockk.dao.getCurrencies() } returns emptyList() andThen emptyList()

            val result = currencyRepository.getCurrencies()
            val actual = result.toList().first().message

            assertEquals("Couldn't load data", actual)
        }
    }


    @Test
    fun testGetConvertedCurrenciesRemote() {
        // Mock the behavior of the repository method
        runBlocking {

            val result = currencyRepository.getConvertedCurrencies()
            val actual = result.toList().first().data?.first()
            val expected = listOfRates.first()

            assertEquals(expected.symbol, actual?.symbol)
            assertEquals(expected.rate, actual?.exchangeRate)

        }
    }

    @Test
    fun testGetConvertedCurrenciesLocal() {
        // Mock the behavior of the repository method
        runBlocking {
            coEvery { dbMockk.dao.getRates() } returns emptyList() andThen listOfRates
            coEvery { dbMockk.dao.clearOldRatesList() } just Runs
            coEvery { dbMockk.dao.updateRates(any()) } just Runs

            val result = currencyRepository.getConvertedCurrencies()
            val actual = result.toList().first().data?.first()
            val expected = listOfRates.first()

            assertEquals(expected.symbol, actual?.symbol)
            assertEquals(expected.rate, actual?.exchangeRate)
        }
    }

    @Test
    fun testGetConvertedCurrenciesError() {
        // Mock the behavior of the repository method
        runBlocking {
            coEvery { dbMockk.dao.getCurrencies() } returns emptyList() andThen emptyList()

            val result = currencyRepository.getConvertedCurrencies()
            val actual = result.toList().first().message

            assertEquals("Couldn't load data", actual)
        }
    }
}
