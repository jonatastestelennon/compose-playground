package com.test.jonnatas

import com.test.jonnatas.data.local.CurrencyEntity
import com.test.jonnatas.data.local.RatesEntity
import com.test.jonnatas.domain.model.ConvertedAmount
import io.mockk.every
import io.mockk.mockk

internal val listOfCurrency: List<CurrencyEntity> = listOf(
    mockk {
        every { name } returns "United States Dollar"
        every { symbol } returns "USD"
    },
    mockk {
        every { name } returns "Euro"
        every { symbol } returns "EUR"
    },
)
internal val listOfRates: List<RatesEntity> = listOf(
    mockk {
        every { rate } returns 1.2
        every { symbol } returns "USD"
    },
    mockk {
        every { rate } returns 3.4
        every { symbol } returns "EUR"
    },
)
internal val listOfAmounts: List<ConvertedAmount> = listOf(
    mockk {
        every { finalMoney } returns 3413.12
        every { exchangeRate } returns 1.2
        every { symbol } returns "USD"
    },
    mockk {
        every { finalMoney } returns 100.12
        every { exchangeRate } returns 3.4
        every { symbol } returns "EUR"
    },
)
