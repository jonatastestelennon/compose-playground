package com.test.jonnatas

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.runtime.Composable
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.test.jonnatas.navigation.Routes
import com.test.jonnatas.presentation.screens.currency.CurrencyScreen
import com.test.jonnatas.presentation.screens.home.HomeScreen
import com.test.jonnatas.presentation.screens.playground.PlaygroundScreen
import com.test.jonnatas.presentation.screens.rickAndMorty.CharactersScreen
import com.test.jonnatas.service.SyncService
import com.test.jonnatas.ui.theme.JonnatasTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    @SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        SyncService.scheduleService(this)
        setContent {
            val navController = rememberNavController()
            JonnatasTheme {
                ProvideNavHost(navController)
            }
        }
    }
}

@Composable
fun ProvideNavHost(navController: NavHostController) {
    NavHost(
        navController = navController, startDestination = Routes.Home.route
    ) {
        composable(Routes.Home.route) {
            HomeScreen(
                NavigationManager.NavigateHome(navController, Routes.Home.route)
            )
        }
        composable(Routes.Currency.route) {
            CurrencyScreen(
                viewModel = hiltViewModel(), navigationManager = NavigationManager.NavigateCurrency(
                    navController, Routes.Character.route
                )
            )
        }
        composable(Routes.Character.route) {
            CharactersScreen(
                viewModel = hiltViewModel(),
                navigationManager = NavigationManager.NavigateCharacter(
                    navController, Routes.Character.route
                )
            )
        }
        composable(Routes.Playground.route) {
            PlaygroundScreen(
                navigationManager = NavigationManager.NavigatePlayground(
                    navController, Routes.Playground.route
                )
            )
        }
    }
}

sealed class NavigationManager(
    val navController: NavHostController,
    val appBarTitle: String,
) {
    fun onBack() {
        navController.popBackStack()
    }

    class NavigatePlayground(navController: NavHostController, appBarTitle: String) :
        NavigationManager(navController, appBarTitle)

    class NavigateCurrency(navController: NavHostController, appBarTitle: String) :
        NavigationManager(navController, appBarTitle)

    class NavigateCharacter(navController: NavHostController, appBarTitle: String) :
        NavigationManager(navController, appBarTitle)

    class NavigateHome(navController: NavHostController, appBarTitle: String) :
        NavigationManager(navController, appBarTitle) {
        fun navigateToCharacter() {
            navController.navigate(Routes.Character.route)
        }

        fun navigateToCurrency() {
            navController.navigate(Routes.Currency.route)
        }

        fun navigateToPlayground() {
            navController.navigate(Routes.Playground.route)
        }
    }
}