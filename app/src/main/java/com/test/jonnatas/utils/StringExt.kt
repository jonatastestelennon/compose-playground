package com.test.jonnatas.utils

fun String.toDoubleMoney(): Double {
    return if (this=="")
        0.0
    else
        this.toDouble()
}