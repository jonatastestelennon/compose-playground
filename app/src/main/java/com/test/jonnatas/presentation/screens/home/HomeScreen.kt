package com.test.jonnatas.presentation.screens.home

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.test.jonnatas.NavigationManager
import com.test.jonnatas.navigation.Routes
import com.test.jonnatas.presentation.components.CustomTopAppBar

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun HomeScreen(
    navigationManager: NavigationManager.NavigateHome,
) {
    Scaffold(
        topBar = {
            CustomTopAppBar(navigationManager)
        },
        content = {
            Surface(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(it),
                color = MaterialTheme.colorScheme.background
            ) {
                Column(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(20.dp),
                    verticalArrangement = Arrangement.spacedBy(30.dp)
                ) {
                    Button(onClick = { navigationManager.navigateToCurrency() }) {
                        Text(text = Routes.Currency.route)
                    }
                    Button(onClick = { navigationManager.navigateToCharacter() }) {
                        Text(text = Routes.Character.route)
                    }
                    Button(onClick = { navigationManager.navigateToPlayground() }) {
                        Text(text = Routes.Playground.route)
                    }
                }
            }
        })
}