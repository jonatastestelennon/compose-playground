package com.test.jonnatas.presentation.screens.rickAndMorty

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.test.jonnatas.data.mapper.toUiState
import com.test.jonnatas.data.remote.resource.Resource
import com.test.jonnatas.domain.use_case.rick.FetchCharactersUseCase
import com.test.jonnatas.domain.use_case.rick.FetchEpisodeByCharactersUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CharacterViewModel @Inject constructor(
    private val fetchCharactersUseCase: FetchCharactersUseCase,
    private val fetchEpisodeByCharactersUseCase: FetchEpisodeByCharactersUseCase,
) : ViewModel() {

    var uiState by mutableStateOf(CharactersUiState())

    init {
        fetchData()
    }

    private fun fetchData() {
        viewModelScope.launch {
            fetchCharactersUseCase.invoke().collect { dataResult ->
                when (dataResult) {
                    is Resource.Error -> {

                    }

                    is Resource.Success -> {
                        dataResult.data?.let { character ->
                            uiState = uiState.copy(listOfCharacters = character.toUiState())
                        }
                    }
                }
            }
        }
    }

    private fun fetchEpisodes() {
        viewModelScope.launch {
            uiState.listOfCharacters.forEachIndexed { index, character ->
                fetchEpisodeByCharactersUseCase.invoke(character.episodesIds)
                    .collect { episodesResult ->
                        when (episodesResult) {
                            is Resource.Error -> {

                            }

                            is Resource.Success -> {
                                episodesResult.data?.let { episodes ->
                                }
                            }
                        }
                    }
            }
        }
    }
}
