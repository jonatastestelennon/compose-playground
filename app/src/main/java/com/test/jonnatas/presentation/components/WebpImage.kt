package com.test.jonnatas.presentation.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp

@Composable
fun WebpImage(resourceId: Int, modifier: Modifier = Modifier) {
    val painter = painterResource(id = resourceId)

    Image(
        painter = painter,
        contentDescription = null, // Provide a content description if needed
        modifier = modifier
            .padding(16.dp)
    )
}