package com.test.jonnatas.presentation.components

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.KeyboardArrowDown
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.unit.toSize
import com.test.jonnatas.R
import com.test.jonnatas.domain.model.Currency

@OptIn(ExperimentalComposeUiApi::class, ExperimentalMaterial3Api::class)
@Composable
fun CustomAutoComplete(
    modifier: Modifier = Modifier,
    menuItems: List<Currency> = emptyList(),
    onFilterText: (String) -> Unit,
    onSelectCurrency: (Currency) -> Unit,
    currencyText: String
) {

    val keyboardController = LocalSoftwareKeyboardController.current

    var selectedCurrencyText by remember(currencyText) {
        mutableStateOf(currencyText)
    }

    var textFieldSize by remember {
        mutableStateOf(Size.Zero)
    }

    var expanded by remember {
        mutableStateOf(true)
    }
    val interactionSource = remember {
        MutableInteractionSource()
    }

    Column(
        modifier = modifier
            .padding(16.dp)
            .fillMaxWidth()
            .clickable(interactionSource = interactionSource, indication = null, onClick = {
                expanded = false
            })
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .clip(
                    MaterialTheme.shapes.medium
                )
        ) {
            Box(modifier = Modifier.fillMaxWidth()) {
                TextField(modifier = Modifier
                    .fillMaxWidth()
                    .height(55.dp)
                    .border(
                        width = 1.8.dp, color = Color.Black, shape = MaterialTheme.shapes.medium
                    )
                    .onGloballyPositioned { coordinates ->
                        textFieldSize = coordinates.size.toSize()
                    }, value = selectedCurrencyText, onValueChange = {
                    selectedCurrencyText = it
                    onFilterText(selectedCurrencyText)
                    expanded = true
                }, colors = TextFieldDefaults.textFieldColors(), textStyle = TextStyle(
                    color = Color.Black, fontSize = 16.sp
                ), keyboardOptions = KeyboardOptions(
                    keyboardType = KeyboardType.Text, imeAction = ImeAction.Done
                ), singleLine = true, trailingIcon = {
                    IconButton(onClick = { expanded = !expanded }) {
                        Icon(
                            modifier = Modifier.size(24.dp),
                            imageVector = Icons.Rounded.KeyboardArrowDown,
                            contentDescription = "arrow",
                            tint = Color.Black
                        )
                    }
                })
                if (selectedCurrencyText.isEmpty()) Text(
                    modifier = Modifier
                        .padding(top = 14.dp, start = 20.dp)
                        .fillMaxSize(),
                    maxLines = 1,
                    text = stringResource(id = R.string.select_currency),
                )
            }

            AnimatedVisibility(visible = expanded) {
                Card(
                    modifier = Modifier
                        .padding(horizontal = 5.dp)
                        .width(textFieldSize.width.dp),
                    shape = RoundedCornerShape(10.dp)
                ) {
                    LazyColumn(
                        modifier = Modifier.heightIn(max = 150.dp),
                    ) {
                        items(menuItems.size) { index ->
                            Row(modifier = Modifier
                                .fillMaxWidth()
                                .clickable {
                                    selectedCurrencyText =
                                        "${menuItems[index].symbol} ${menuItems[index].name}"
                                    onSelectCurrency(menuItems[index])
                                    expanded = false
                                    keyboardController?.hide()
                                }
                                .padding(horizontal = 10.dp)) {
                                Text(
                                    modifier = Modifier.padding(8.dp),
                                    text = menuItems[index].symbol,
                                )
                                Text(
                                    text = menuItems[index].name, modifier = Modifier.padding(8.dp)
                                )
                            }
                        }
                    }
                }
            }
        }
    }
}