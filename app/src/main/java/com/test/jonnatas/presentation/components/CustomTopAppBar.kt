package com.test.jonnatas.presentation.components

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import com.test.jonnatas.NavigationManager

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CustomTopAppBar(
    navigationManager: NavigationManager,
) {
    TopAppBar(
        title = { Text(text = navigationManager.appBarTitle) },
        navigationIcon = {
            IconButton(onClick = {
                navigationManager.onBack()
            }) {
                Icon(Icons.Filled.ArrowBack, "backIcon")
            }
        },
    )
}