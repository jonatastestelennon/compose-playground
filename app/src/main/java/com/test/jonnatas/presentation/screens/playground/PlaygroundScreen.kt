package com.test.jonnatas.presentation.screens.playground

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.rememberUpdatedState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.unit.dp
import com.test.jonnatas.NavigationManager
import com.test.jonnatas.R
import com.test.jonnatas.presentation.components.CustomTopAppBar
import com.test.jonnatas.presentation.components.WebpImage
import kotlinx.coroutines.launch

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun PlaygroundScreen(
    navigationManager: NavigationManager.NavigatePlayground,
) {
    // Basic remember function is used to store and retrieve a value during recomposition.
    // It's commonly used for non-UI-related state.
    var count by remember { mutableIntStateOf(0) }

    /*  rememberSaveable is used to store and retrieve a value during recomposition,
        and it also supports automatic state restoration across configuration changes.
     */
    var countSaveable by rememberSaveable { mutableIntStateOf(0) }

    /*
        rememberUpdatedState is used to create a state holder that only updates if the value passed
        to it changes. helpful for effects that contain long-lived operations that may be expensive
        or prohibitive to recreate and restart.
     */
    val updatedParam by rememberUpdatedState(countSaveable)

    /*
    rememberCoroutineScope is used to create a coroutine scope that is tied to the lifecycle of the
    composable. It is useful for launching coroutines in a composable.
     */
    val coroutineScope = rememberCoroutineScope()


    Scaffold(
        topBar = {
        CustomTopAppBar(navigationManager)
    }, content = {
        Surface(
            modifier = Modifier
                .fillMaxSize()
                .padding(it),
            color = MaterialTheme.colorScheme.background
        ) {
            Text("Press me")
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(20.dp),
                verticalArrangement = Arrangement.spacedBy(30.dp)
            ) {
                Text("Remeber value: $count")
                Text("countSaveable value: $countSaveable")
                Text("updatedParam value: $updatedParam")

                Button(onClick = {
                    coroutineScope.launch {
                        count++
                        countSaveable++
                    }
                }) {
                    Text("Press sasa me")
                }

                AnimatedVisibility(visible = count > 10) {
                    WebpImage(
                        resourceId = R.drawable.image,
                        modifier = Modifier
                            .size(500.dp)
                            .clip(MaterialTheme.shapes.extraLarge),
                    )
                }
            }
        }
    })
}