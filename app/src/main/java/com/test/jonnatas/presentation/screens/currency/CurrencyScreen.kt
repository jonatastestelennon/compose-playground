package com.test.jonnatas.presentation.screens.currency

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Snackbar
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.test.jonnatas.NavigationManager
import com.test.jonnatas.R
import com.test.jonnatas.presentation.components.CustomAutoComplete
import com.test.jonnatas.presentation.components.CustomTextField
import com.test.jonnatas.presentation.components.CustomTopAppBar
import kotlinx.coroutines.launch

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CurrencyScreen(
    viewModel: CurrencyViewModel,
    navigationManager: NavigationManager.NavigateCurrency,
) {
    val uiState = viewModel.state
    ErrorSnackbar(uiState.errorMessage)
    Scaffold(
        topBar = {
            CustomTopAppBar(navigationManager)
        },
        content = {
            Surface(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(it),
                color = MaterialTheme.colorScheme.background
            ) {
                Column(
                    modifier = Modifier.fillMaxSize()
                ) {
                    CustomTextField(
                        modifier = Modifier.padding(16.dp),
                        label = stringResource(id = R.string.currency_label),
                        onValueChange = { value ->
                            viewModel.changeValueEvent(value)
                        },
                    )
                    CustomAutoComplete(menuItems = uiState.listOfCurrency,
                        currencyText = uiState.selectedCurrencyText,
                        onFilterText = { queryText ->
                            viewModel.filterCurrencyEvent(queryText)
                        },
                        onSelectCurrency = { currency ->
                            viewModel.changeCurrencyEvent(currency)
                        })
                    LazyVerticalGrid(contentPadding = PaddingValues(16.dp),
                        modifier = Modifier
                            .fillMaxWidth()
                            .fillMaxHeight()
                            .clip(shape = RoundedCornerShape(16.dp, 16.dp, 0.dp, 0.dp))
                            .background(color = MaterialTheme.colorScheme.secondary)
                            .clip(
                                RoundedCornerShape(12.dp, 12.dp, 0.dp, 0.dp)
                            ),
                        columns = GridCells.Fixed(3),
                        content = {
                            items(uiState.convertedAmount) { currency ->
                                CurrencyComponent(
                                    symbol = currency.symbol, value = currency.finalMoney
                                )
                            }
                        })
                }
            }
        }
    )
}

@Composable
fun ErrorSnackbar(errorMessage: String) {
    if (errorMessage.isNotBlank()) {
        val snackbarHostState = remember { SnackbarHostState() }
        val coroutineScope = rememberCoroutineScope()

        // Display the Snackbar
        LaunchedEffect(errorMessage) {
            coroutineScope.launch {
                snackbarHostState.showSnackbar(
                    message = errorMessage,
                    actionLabel = "Dismiss"
                )
            }
        }

        // SnackbarHost to display the Snackbar
        SnackbarHost(
            hostState = snackbarHostState,
            modifier = Modifier.fillMaxSize()
        ) { snackbarData ->
            Snackbar(
                snackbarData = snackbarData,
                modifier = Modifier.padding(16.dp)
            )
        }
    }
}