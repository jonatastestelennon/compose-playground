package com.test.jonnatas.presentation.screens.rickAndMorty

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import com.test.jonnatas.NavigationManager
import com.test.jonnatas.presentation.components.CustomTopAppBar
import com.test.jonnatas.presentation.screens.currency.ErrorSnackbar

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CharactersScreen(
    viewModel: CharacterViewModel,
    navigationManager: NavigationManager.NavigateCharacter,
) {
    val uiState = viewModel.uiState
    ErrorSnackbar(uiState.errorMessage)
    Scaffold(
        topBar = {
            CustomTopAppBar(navigationManager)
        },
        content = { screenPadding ->
            Surface(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(screenPadding),
                color = MaterialTheme.colorScheme.background
            ) {
                LazyColumn(modifier = Modifier.fillMaxHeight().padding(23.dp).background(MaterialTheme.colorScheme.onBackground)) {
                    items(uiState.listOfCharacters) {
                        CharacterItem(
                            name = it.name, image = it.image
                        )
                    }
                }
            }
        })
}

@Composable
fun CharacterItem(
    modifier: Modifier = Modifier, name: String, image: String
) {
    Column(modifier = modifier.padding(10.dp)) {
        Row(
            modifier = Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically
        ) {
            AsyncImage(
                modifier = Modifier
                    .size(100.dp)
                    .clip(MaterialTheme.shapes.extraLarge),
                model = image,
                contentDescription = name
            )
            Text(
                modifier = Modifier.fillMaxWidth(),
                style = MaterialTheme.typography.titleLarge,
                text = name,
                textAlign = TextAlign.Center
            )
        }
    }
}