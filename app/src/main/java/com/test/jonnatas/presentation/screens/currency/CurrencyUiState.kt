package com.test.jonnatas.presentation.screens.currency

import com.test.jonnatas.domain.model.ConvertedAmount
import com.test.jonnatas.domain.model.Currency

data class CurrencyUiState(
    val listOfCurrency: List<Currency> = emptyList(),
    val convertedAmount: List<ConvertedAmount> = emptyList(),
    val selectedCurrency: Currency = Currency(),
    val selectedCurrencyText: String = "",
    val valueToConvert: Double = 0.0,
    val errorMessage: String = ""
)
