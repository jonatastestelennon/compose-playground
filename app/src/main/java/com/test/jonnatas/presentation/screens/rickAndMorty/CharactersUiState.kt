package com.test.jonnatas.presentation.screens.rickAndMorty

data class CharactersUiState(
    val listOfCharacters: List<CharacterUiState> = emptyList(),
    val errorMessage: String = ""
)

data class CharacterUiState(
    val name: String,
    val image: String,
    val episodesIds: List<String>,
    val episodes: List<EpisodesUiState>,
)

data class EpisodesUiState(
    val name: String,
    val imageUrl: String,
    val episode: String,
)