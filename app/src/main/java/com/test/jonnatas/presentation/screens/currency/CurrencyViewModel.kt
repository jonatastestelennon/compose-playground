package com.test.jonnatas.presentation.screens.currency

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.test.jonnatas.data.remote.resource.Resource
import com.test.jonnatas.domain.model.Currency
import com.test.jonnatas.domain.use_case.currency.ConvertCurrenciesUseCase
import com.test.jonnatas.domain.use_case.currency.FilterCurrencyUseCase
import com.test.jonnatas.domain.use_case.currency.GetCurrencyUseCase
import com.test.jonnatas.utils.toDoubleMoney
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CurrencyViewModel @Inject constructor(
    private val getCurrenciesUseCase: GetCurrencyUseCase,
    private val convertCurrenciesUseCase: ConvertCurrenciesUseCase,
    private val filterCurrencyUseCase: FilterCurrencyUseCase
) : ViewModel() {
    var state by mutableStateOf(CurrencyUiState())

    init {
        getCurrencies()
    }

    fun changeValueEvent(value: String) {
        state = state.copy(
            valueToConvert = value.toDoubleMoney()
        )
        getConvertCurrencies()
    }

    fun changeCurrencyEvent(currency: Currency) {
        if (state.selectedCurrency != currency) {
            state = state.copy(
                selectedCurrency = currency
            )
            getConvertCurrencies()
        }
    }

    fun filterCurrencyEvent(currency: String) {
        viewModelScope.launch {
            filterCurrencyUseCase(query = currency).collect { result ->
                when (result) {
                    is Resource.Error -> {
                        result.message?.let { message ->
                            state.copy(errorMessage = message)
                        }
                    }

                    is Resource.Success -> {
                        result.data?.let {
                            state = state.copy(listOfCurrency = it)
                        }
                    }
                }
            }
        }
    }

    private fun getCurrencies() {
        viewModelScope.launch {
            getCurrenciesUseCase().collect { result ->
                if (result is Resource.Error) {
                    result.message?.let { message ->
                        state.copy(errorMessage = message)
                    }
                }
            }
        }
    }

    private fun getConvertCurrencies() {
        viewModelScope.launch {
            convertCurrenciesUseCase(
                amount = state.valueToConvert, symbol = state.selectedCurrency.symbol
            ).collect { result ->
                when (result) {
                    is Resource.Error -> {
                        result.message?.let { message ->
                            state.copy(errorMessage = message)
                        }
                    }

                    is Resource.Success -> {
                        result.data?.let {
                            state = state.copy(
                                convertedAmount = it
                            )
                        }
                    }
                }
            }
        }
    }
}