package com.test.jonnatas.navigation

import androidx.compose.runtime.Immutable
import androidx.compose.runtime.Stable

@Stable
@Immutable
sealed interface Routes {
    val route: String

    object Character : Routes {
        override val route: String
            get() = "Character"
    }

    object Currency : Routes {
        override val route: String
            get() = "Currency"
    }

    object Home : Routes {
        override val route: String
            get() = "Home"
    }

    object Playground : Routes {
        override val route: String
            get() = "Playground"
    }
}
