package com.test.jonnatas.domain.use_case.rick

import com.test.jonnatas.data.remote.resource.Resource
import com.test.jonnatas.domain.model.Episode
import com.test.jonnatas.domain.repository.RickRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class FetchEpisodeByCharactersUseCase @Inject constructor(
    private val repository: RickRepository
) {

    suspend operator fun invoke(episodesList: List<String>): Flow<Resource<List<Episode>>> {
        return repository.getEpisodesOfCharacter(episodesList)
    }
}