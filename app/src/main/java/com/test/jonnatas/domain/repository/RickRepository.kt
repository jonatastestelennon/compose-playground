package com.test.jonnatas.domain.repository

import com.test.jonnatas.data.remote.resource.Resource
import com.test.jonnatas.domain.model.Characters
import com.test.jonnatas.domain.model.Episode
import kotlinx.coroutines.flow.Flow

interface RickRepository {

    suspend fun getCharacters(): Flow<Resource<List<Characters>>>

    suspend fun getEpisodesOfCharacter(episodesList: List<String>): Flow<Resource<List<Episode>>>
}