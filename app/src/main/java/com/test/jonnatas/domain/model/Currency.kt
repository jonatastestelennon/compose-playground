package com.test.jonnatas.domain.model

data class Currency(val symbol: String = "", val name: String = "")
