package com.test.jonnatas.domain.use_case.currency

import com.test.jonnatas.data.remote.resource.Resource
import com.test.jonnatas.domain.model.ConvertedAmount
import com.test.jonnatas.domain.model.Rates
import com.test.jonnatas.domain.repository.CurrencyRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class ConvertCurrenciesUseCase @Inject constructor(
    private val repository: CurrencyRepository,
) {

    suspend operator fun invoke(
        amount: Double, symbol: String
    ): Flow<Resource<List<ConvertedAmount>>> {
        val currencyRatesFlow = repository.getConvertedCurrencies()

        return currencyRatesFlow.map { resource ->
            when (resource) {
                is Resource.Success -> {
                    val fromCurrencyRate: Rates? =
                        resource.data?.firstOrNull { it.symbol == symbol }

                    if (fromCurrencyRate == null) {
                        Resource.Error(
                            message = "Sorry, we cant convert now. Try Again latter.", null
                        )
                    } else {
                        val convertedCurrencies = resource.data.map {
                            ConvertedAmount(
                                symbol = it.symbol,
                                exchangeRate = it.exchangeRate,
                                finalMoney = convertCurrency(
                                    amount = amount,
                                    fromCurrencyRate = fromCurrencyRate.exchangeRate,
                                    toCurrencyRate = it.exchangeRate
                                )
                            )
                        }
                        Resource.Success(convertedCurrencies)
                    }
                }

                is Resource.Error -> {
                    Resource.Error(
                        message = "Sorry, we cant convert now. Try Again latter.", null
                    ) // You may need to adapt this part
                }
            }
        }
    }

    /**
     * Converts an amount to dollars using the specified exchange rate.
     * example 0.93 // 1 euro = 0.93 dollars
     *
     * @param amount The amount to be converted to dollars.
     * @param exchangeRate The exchange rate, where 1 [amount] is equivalent to [exchangeRate] dollars.
     * @return The equivalent amount in dollars.
     */
    private fun convertAmountToDollar(amount: Double, exchangeRate: Double): Double {
        return amount / exchangeRate
    }

    /**
     * Converts dollars to a specified exchange rate.
     *
     * @param usdAmount The amount to be converted to dollars.
     * @param finalRate The expected amount in the final currency.
     * @return The equivalent amount in the final currency.
     */
    private fun convertDollarToTargetCurrency(usdAmount: Double, finalRate: Double): Double {
        return usdAmount * finalRate
    }

    /**
     * Converts an amount from one currency to another using exchange rates.
     *
     * This function takes an initial amount, the exchange rate of the source currency to a common base currency,
     * and the exchange rate of the target currency to the same base currency, and calculates the equivalent
     * amount in the target currency.
     *
     * @param amount           The amount to be converted, specified in the source currency.
     * @param fromCurrencyRate The exchange rate of the source currency to the base currency.
     * @param toCurrencyRate   The exchange rate of the target currency to the base currency.
     * @return                 The equivalent amount in the target currency.
     */
    private fun convertCurrency(
        amount: Double, fromCurrencyRate: Double, toCurrencyRate: Double
    ): Double {
        val usdAmount = convertAmountToDollar(amount, fromCurrencyRate)
        return convertDollarToTargetCurrency(usdAmount, toCurrencyRate)
    }
}