package com.test.jonnatas.domain.use_case.currency

import com.test.jonnatas.data.remote.resource.Resource
import com.test.jonnatas.domain.model.Currency
import com.test.jonnatas.domain.repository.CurrencyRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class FilterCurrencyUseCase @Inject constructor(
    private val repository: CurrencyRepository,
) {

    suspend operator fun invoke(query: String): Flow<Resource<List<Currency>>> {
        return repository.filterCurrency(query)
    }
}