package com.test.jonnatas.domain.model

data class ConvertedAmount(val symbol: String, val exchangeRate: Double, val finalMoney: Double)
