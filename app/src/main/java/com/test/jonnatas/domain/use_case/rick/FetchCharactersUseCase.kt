package com.test.jonnatas.domain.use_case.rick

import com.test.jonnatas.data.remote.resource.Resource
import com.test.jonnatas.domain.model.Characters
import com.test.jonnatas.domain.repository.RickRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class FetchCharactersUseCase @Inject constructor(
    private val repository: RickRepository
) {

    suspend operator fun invoke(): Flow<Resource<List<Characters>>> {
        return repository.getCharacters()
    }
}