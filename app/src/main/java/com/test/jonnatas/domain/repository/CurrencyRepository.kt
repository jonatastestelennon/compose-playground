package com.test.jonnatas.domain.repository

import com.test.jonnatas.data.remote.resource.Resource
import com.test.jonnatas.domain.model.Currency
import com.test.jonnatas.domain.model.Rates
import kotlinx.coroutines.flow.Flow

interface CurrencyRepository {

    suspend fun syncData(
    ): Flow<Resource<List<Rates>>>

    suspend fun getConvertedCurrencies(): Flow<Resource<List<Rates>>>

    suspend fun filterCurrency(query: String): Flow<Resource<List<Currency>>>

    suspend fun getCurrencies(): Flow<Resource<List<Currency>>>
}