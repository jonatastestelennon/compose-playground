package com.test.jonnatas.domain.model

data class Rates(val symbol: String, val exchangeRate: Double)
