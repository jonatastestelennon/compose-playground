package com.test.jonnatas.domain.model

data class Characters(
    val name: String,
    val image: String,
    val episodes: List<String>
)
