package com.test.jonnatas.di

import android.app.Application
import android.content.Context
import androidx.room.Room
import androidx.work.WorkManager
import com.test.jonnatas.BuildConfig
import com.test.jonnatas.data.local.CurrencyDatabase
import com.test.jonnatas.data.local.CurrencyDatabase.Companion.NAME_DATABASE
import com.test.jonnatas.data.remote.api.CurrencyApi
import com.test.jonnatas.data.remote.api.RickApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.create
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DataModule {

    @Provides
    @Singleton
    fun provideCurrencyApi(): CurrencyApi {
        return Retrofit.Builder().baseUrl(BuildConfig.EXCHANGE_API)
            .addConverterFactory(MoshiConverterFactory.create()).client(
                OkHttpClient.Builder().addInterceptor(HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.BASIC
                }).build()
            ).build().create()
    }

    @Provides
    @Singleton
    fun provideRickApi(): RickApi {
        return Retrofit.Builder().baseUrl(BuildConfig.RICK_API)
            .addConverterFactory(MoshiConverterFactory.create()).client(
                OkHttpClient.Builder().addInterceptor(HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.BASIC
                }).build()
            ).build().create()
    }

    @Provides
    @Singleton
    fun provideDatabase(app: Application): CurrencyDatabase {
        return Room.databaseBuilder(
            app, CurrencyDatabase::class.java, NAME_DATABASE
        ).build()
    }

    @Singleton
    @Provides
    fun provideWorkManager(@ApplicationContext context: Context): WorkManager =
        WorkManager.getInstance(context)
}