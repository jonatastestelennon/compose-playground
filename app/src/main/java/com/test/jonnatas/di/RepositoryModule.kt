package com.test.jonnatas.di

import com.test.jonnatas.data.repository.CurrencyRepositoryImpl
import com.test.jonnatas.data.repository.RickRepositoryImpl
import com.test.jonnatas.domain.repository.CurrencyRepository
import com.test.jonnatas.domain.repository.RickRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {
    @Binds
    @Singleton
    abstract fun bindCurrencyRepository(
        currencyRepositoryImpl: CurrencyRepositoryImpl
    ): CurrencyRepository

    @Binds
    @Singleton
    abstract fun bindRickRepository(
        rickRepositoryImpl: RickRepositoryImpl
    ): RickRepository
}
