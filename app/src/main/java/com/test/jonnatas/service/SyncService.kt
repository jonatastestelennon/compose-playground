package com.test.jonnatas.service

import android.app.AlarmManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.IBinder
import android.os.SystemClock
import com.test.jonnatas.domain.use_case.currency.SyncCurrenciesUseCase
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class SyncService : Service() {
    @Inject
    lateinit var syncUseCase: SyncCurrenciesUseCase

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        // Fetch data using the injected repository
        CoroutineScope(Dispatchers.IO).launch {
            syncUseCase().collect()
        }
        // Return START_STICKY to ensure the service is restarted if it's killed by the system
        return START_STICKY
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    companion object {
        // 30 * 60 * 1000 = 1800000 // 30 minutes in milliseconds
        private const val SYNC_INTERVAL = 1800000L

        fun scheduleService(context: Context, intervalMillis: Long = SYNC_INTERVAL) {
            val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            val intent = Intent(context, SyncService::class.java)
            val pendingIntent = PendingIntent.getService(
                context,
                1233,
                intent,
                PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
            )

            alarmManager.setRepeating(
                AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + intervalMillis,
                intervalMillis,
                pendingIntent
            )
        }
    }
}