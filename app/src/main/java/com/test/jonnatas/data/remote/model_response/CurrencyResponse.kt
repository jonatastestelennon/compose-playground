package com.test.jonnatas.data.remote.model_response

data class LatestCurrencyResponse(
    val timestamp: Long,
    val base: String,
    val rates: Map<String, Double>
)