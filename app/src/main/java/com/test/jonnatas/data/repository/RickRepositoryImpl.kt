package com.test.jonnatas.data.repository

import com.test.jonnatas.data.mapper.toCharacter
import com.test.jonnatas.data.mapper.toEpisodeList
import com.test.jonnatas.data.remote.api.RickApi
import com.test.jonnatas.data.remote.resource.Resource
import com.test.jonnatas.domain.model.Characters
import com.test.jonnatas.domain.model.Episode
import com.test.jonnatas.domain.repository.RickRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class RickRepositoryImpl @Inject constructor(private val api: RickApi) : RickRepository {
    override suspend fun getCharacters(): Flow<Resource<List<Characters>>> = flow {
        try {
            val data = api.getCharacter()
            emit(Resource.Success(data = data.toCharacter()))
        } catch (e: Exception) {
            emit(Resource.Error(message = "Couldn't load data"))
        }
    }

    override suspend fun getEpisodesOfCharacter(episodesList: List<String>): Flow<Resource<List<Episode>>> =
        flow {
            try {
                val data = api.getEpisodesOfCharacter(episodesList)
                emit(Resource.Success(data = data.toEpisodeList()))
            } catch (e: Exception) {
                emit(Resource.Error(message = "Sorry, try again later"))
            }
        }
}