package com.test.jonnatas.data.local

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(
    entities = [CurrencyEntity::class, RatesEntity::class], version = 2
)
abstract class CurrencyDatabase : RoomDatabase() {
    abstract val dao: CurrencyDao

    companion object {
        const val NAME_DATABASE = "currency.db"
    }
}