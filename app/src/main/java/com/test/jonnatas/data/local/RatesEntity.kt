package com.test.jonnatas.data.local

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class RatesEntity(
    @PrimaryKey(autoGenerate = true) val id: Long = 0, val symbol: String, val rate: Double
)