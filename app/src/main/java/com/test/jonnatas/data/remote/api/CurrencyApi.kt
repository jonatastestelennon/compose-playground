package com.test.jonnatas.data.remote.api

import com.test.jonnatas.data.remote.model_response.LatestCurrencyResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface CurrencyApi {

    @GET("/currencies.json")
    suspend fun getCurrencies(): Map<String, String>

    @GET("/latest.json")
    suspend fun getConvertedCurrencies(
        @Query("app_id") appID: String = APP_ID,
    ): LatestCurrencyResponse


    companion object {
        const val APP_ID = "4fcaf7a8cd8142998415b8f012c4b09c"
    }
}