package com.test.jonnatas.data.mapper

import com.test.jonnatas.data.local.CurrencyEntity
import com.test.jonnatas.data.local.RatesEntity
import com.test.jonnatas.data.remote.model_response.LatestCurrencyResponse
import com.test.jonnatas.domain.model.Currency
import com.test.jonnatas.domain.model.Rates

fun Map<String, String>.toListOfCurrency(): List<Currency> {
    val list = mutableListOf<Currency>()
    this.forEach { (symbol, name) ->
        list.add(Currency(name = name, symbol = symbol))
    }
    return list
}

fun LatestCurrencyResponse.toListOfCurrency(): List<Rates> {
    val list = mutableListOf<Rates>()
    this.rates.forEach { (symbol, exchangeRate) ->
        list.add(Rates(symbol = symbol, exchangeRate = exchangeRate))
    }

    return list
}

fun List<RatesEntity>.toListOfRates(): List<Rates> {
    val list = mutableListOf<Rates>()
    this.forEach { rate ->
        list.add(Rates(symbol = rate.symbol, exchangeRate = rate.rate))
    }

    return list
}

fun List<CurrencyEntity>.toListOfCurrency(): List<Currency> {
    val list = mutableListOf<Currency>()
    this.forEach { entity ->
        list.add(Currency(name = entity.name, symbol = entity.symbol))
    }
    return list
}

fun List<Rates>.toListOfRatesEntity(): List<RatesEntity> {
    val list = mutableListOf<RatesEntity>()
    this.forEach { entity ->
        list.add(RatesEntity(symbol = entity.symbol, rate = entity.exchangeRate))
    }
    return list
}

fun List<Currency>.toListOfCurrencyEntity(): List<CurrencyEntity> {
    val list = mutableListOf<CurrencyEntity>()
    this.forEach { entity ->
        list.add(CurrencyEntity(name = entity.name, symbol = entity.symbol))
    }
    return list
}