package com.test.jonnatas.data.remote.model_response

import com.squareup.moshi.Json

data class EpisodeResponse(
    val id: Int,
    val name: String?,
    @field:Json(name = "air_date")
    val airDate: String?,
    val episode: String?,
    val characters: List<String>?,
    @field:Json(name = "url")
    val imageUrl: String?
)
