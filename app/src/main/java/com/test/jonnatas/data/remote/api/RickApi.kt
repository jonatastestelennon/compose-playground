package com.test.jonnatas.data.remote.api

import com.test.jonnatas.data.remote.model_response.CharacterResponse
import com.test.jonnatas.data.remote.model_response.EpisodeResponse
import retrofit2.http.GET
import retrofit2.http.Path

interface RickApi {

    @GET("character/")
    suspend fun getCharacter(): CharacterResponse

    @GET("episode/{episodes}")
    suspend fun getEpisodesOfCharacter(
        @Path(value = "episodes") episodesList: List<String>
    ): List<EpisodeResponse>
}