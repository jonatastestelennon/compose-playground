package com.test.jonnatas.data.repository

import com.test.jonnatas.data.local.CurrencyDatabase
import com.test.jonnatas.data.mapper.toListOfCurrency
import com.test.jonnatas.data.mapper.toListOfCurrencyEntity
import com.test.jonnatas.data.mapper.toListOfRates
import com.test.jonnatas.data.mapper.toListOfRatesEntity
import com.test.jonnatas.data.remote.api.CurrencyApi
import com.test.jonnatas.data.remote.resource.Resource
import com.test.jonnatas.domain.model.Currency
import com.test.jonnatas.domain.model.Rates
import com.test.jonnatas.domain.repository.CurrencyRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CurrencyRepositoryImpl @Inject constructor(
    private val api: CurrencyApi,
    private val db: CurrencyDatabase,
) : CurrencyRepository {

    override suspend fun syncData(): Flow<Resource<List<Rates>>> {
        return flow {
            try {
                val currency = api.getCurrencies().toListOfCurrency()
                val rates = api.getConvertedCurrencies().toListOfCurrency()
                persistCurrencies(currency)
                persistRates(rates)
                emit(Resource.Success(data = getUpdatedRates()))
            } catch (e: Exception) {
                emit(Resource.Error("Couldn't load data"))
            }
        }
    }

    override suspend fun getCurrencies(): Flow<Resource<List<Currency>>> {
        return flow {
            try {
                val localData = db.dao.getCurrencies()
                if (localData.isEmpty()) {
                    val response = api.getCurrencies().toListOfCurrency()
                    persistCurrencies(response)
                    emit(Resource.Success(getUpdatedCurrencies()))
                } else {
                    emit(Resource.Success(data = localData.toListOfCurrency()))
                }
            } catch (e: Exception) {
                emit(Resource.Error("Couldn't load data"))
            }
        }
    }

    override suspend fun getConvertedCurrencies(): Flow<Resource<List<Rates>>> {
        return flow {
            try {
                val localData = db.dao.getRates()
                if (localData.isEmpty()) {
                    val response = api.getConvertedCurrencies().toListOfCurrency()
                    persistRates(response)
                    emit(Resource.Success(getUpdatedRates()))
                } else {
                    emit(Resource.Success(data = localData.toListOfRates()))
                }
            } catch (e: Exception) {
                emit(Resource.Error("Couldn't load data"))
            }
        }
    }

    override suspend fun filterCurrency(query: String): Flow<Resource<List<Currency>>> {
        return flow {
            val queryData = db.dao.searchCurrency("%$query%")
            emit(Resource.Success(queryData))
        }
    }

    private suspend fun getUpdatedRates(): List<Rates> {
        return db.dao.getRates().toListOfRates()
    }

    private suspend fun getUpdatedCurrencies(): List<Currency> {
        return db.dao.getCurrencies().toListOfCurrency()
    }

    private suspend fun persistCurrencies(response: List<Currency>) {
        db.dao.clearOldCurrencyList()
        db.dao.updateWithNewCurrencies(response.toListOfCurrencyEntity())
    }

    private suspend fun persistRates(response: List<Rates>) {
        db.dao.clearOldRatesList()
        db.dao.updateRates(response.toListOfRatesEntity())
    }
}