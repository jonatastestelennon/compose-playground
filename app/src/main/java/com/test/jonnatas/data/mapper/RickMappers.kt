package com.test.jonnatas.data.mapper

import com.test.jonnatas.data.remote.model_response.CharacterResponse
import com.test.jonnatas.data.remote.model_response.EpisodeResponse
import com.test.jonnatas.domain.model.Characters
import com.test.jonnatas.domain.model.Episode
import com.test.jonnatas.presentation.screens.rickAndMorty.CharacterUiState

fun List<Characters>.toUiState(): List<CharacterUiState> {
    val resultList = mutableListOf<CharacterUiState>()
    this.map {
        resultList.add(
            CharacterUiState(
                name = it.name, image = it.image, episodesIds = it.episodes, episodes = emptyList()
            )
        )
    }
    return resultList
}

fun CharacterResponse.toCharacter(): List<Characters> {
    val resultList = mutableListOf<Characters>()
    results.map {
        resultList.add(
            Characters(
                name = it.name.orEmpty(),
                image = it.image.orEmpty(),
                episodes = it.episodes ?: emptyList()
            )
        )
    }
    return resultList
}

fun List<EpisodeResponse>.toEpisodeList(): List<Episode> {
    val resultList = mutableListOf<Episode>()
    this.map {
        resultList.add(
            Episode(
                id = it.id,
                name = it.name.orEmpty(),
                episode = it.episode.orEmpty(),
                airDate = it.airDate.orEmpty(),
                imageUrl = it.imageUrl.orEmpty(),
                characters = it.characters ?: emptyList()
            )
        )
    }
    return resultList
}