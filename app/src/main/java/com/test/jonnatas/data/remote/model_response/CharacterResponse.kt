package com.test.jonnatas.data.remote.model_response

data class CharacterResponse(
    val results: List<CharacterResultResponse>
)
