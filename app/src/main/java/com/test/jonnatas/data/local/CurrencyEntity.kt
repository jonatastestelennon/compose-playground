package com.test.jonnatas.data.local

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class CurrencyEntity(
    @PrimaryKey val symbol: String, val name: String
)