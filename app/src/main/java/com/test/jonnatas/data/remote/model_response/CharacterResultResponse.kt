package com.test.jonnatas.data.remote.model_response

import com.squareup.moshi.Json

data class CharacterResultResponse(
    val name: String? = "",
    val image: String? = "",
    @field:Json(name = "episode") val episodes: List<String>? = emptyList()
)