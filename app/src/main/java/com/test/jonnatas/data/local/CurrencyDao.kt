package com.test.jonnatas.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.test.jonnatas.domain.model.Currency

@Dao
interface CurrencyDao {

    @Query("SELECT * FROM ratesentity")
    suspend fun getRates(): List<RatesEntity>

    @Query("DELETE FROM ratesentity")
    suspend fun clearOldRatesList()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateRates(ratesEntity: List<RatesEntity>)

    @Query("SELECT * FROM currencyentity")
    suspend fun getCurrencies(): List<CurrencyEntity>

    @Query("DELETE FROM currencyentity")
    suspend fun clearOldCurrencyList()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateWithNewCurrencies(currencyEntities: List<CurrencyEntity>)

    @Query("SELECT * FROM currencyentity WHERE name LIKE :query LIMIT 5")
    suspend fun searchCurrency(query: String): List<Currency>
}